import os
import time
import datetime
from decimal import Decimal
from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, YEAR INT, TITLE TEXT, DIRECTOR TEXT, ACTOR TEXT, RELEASE_DATE TEXT, RATING DECIMAL(10,1), PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT year, title, director, actor, release_date, rating FROM movies")
    entries = []
    row = cur.fetchone()
    while row is not None:
        rowtext = ", ".join([str(x) for x in row])
        entries.append(rowtext)
        row = cur.fetchone()
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    actor = request.form['actor']
    director = request.form['director']
    release_date = request.form['release_date']
    rating = request.form['rating']

    rating = Decimal(rating)
    messages = []
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    selquery = "SELECT title FROM movies WHERE title = '%s'" % (title)
    cur.execute(selquery)
    row_count = cur.rowcount
    message = ""
    if(row_count == 0):
        query = """INSERT INTO movies (year, title, director, actor, release_date, rating) VALUES ('%s','%s','%s','%s','%s','%.1f')""" % (year,title,director,actor,release_date,rating)
        try:
            cur.execute(query)
            cnx.commit()
        except (MySQLdb.Error, MySQLdb.Warning) as e:
            message = "Movie %s could not be inserted - %s" % (title, e)
        message = "Movie %s successfully inserted" % title
    else:
        message = "Movie %s already exists" % title
    entries = query_data()
    messages.append(message)
    return render_template('index.html', entries = entries, messages = messages)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    actor = request.form['actor']
    director = request.form['director']
    release_date = request.form['release_date']
    rating = request.form['rating']

    rating = Decimal(rating)
    messages = []
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    selquery = "SELECT title FROM movies WHERE title = '%s'" % (title)
    cur.execute(selquery)
    row_count = cur.rowcount
    message = ""
    if(row_count != 0):
        query = "UPDATE movies SET year = '%s', title = '%s', director = '%s', actor = '%s', release_date = '%s', rating = '%.1f' WHERE title = '%s'" % (year,title,director,actor,release_date,rating, title)
        try:
            cur.execute(query)
            cnx.commit()
        except (MySQLdb.Error, MySQLdb.Warning) as e:
            message = "Movie %s could not be updated - %s" % (title, e)
        message = "Movie %s successfully updated" % title
    else:
        message = "Movie %s does not exist" % title
    entries = query_data()
    messages.append(message)
    return render_template('index.html', entries = entries, messages = messages)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")
    title = request.form['delete_title']
    messages = []
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    selquery = "SELECT title FROM movies WHERE title = '%s'" % (title)
    cur.execute(selquery)
    row_count = cur.rowcount
    query = "DELETE FROM movies WHERE title = '%s'" % (title)
    message = ""
    if(row_count == 0):
        message = "Movie with %s does not exist" % (title)
    else:
        try:
            cur.execute(query)
            cnx.commit()
        except (MySQLdb.Error, MySQLdb.Warning) as e:
            message = "Movie %s could not be deleted - %s" % (title, e)
        message = "Movie %s successfully deleted" % title
    entries = query_data()
    messages.append(message)
    return render_template('index.html', entries = entries, messages = messages)

@app.route('/search_movie', methods=['POST'])
def search_movie():
    print("Received request.")
    actor = request.form['search_actor']
    messages = []
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    selquery = "SELECT title, year, actor FROM movies WHERE actor = '%s'" % (actor)
    message = ""
    try:
        cur.execute(selquery)
        cnx.commit()
    except (MySQLdb.Error, MySQLdb.Warning) as e:
        message = e
    row_count = cur.rowcount
    if(row_count == 0):
        message = "No movies found for actor %s" % (actor)
        messages.append(message)
    else:
        row = cur.fetchone()
        while row is not None:
            rowtext = ", ".join([str(x) for x in row])
            messages.append(rowtext)
            row = cur.fetchone()
    entries = query_data()
    return render_template('index.html', entries = entries, messages = messages)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()
    messages = []
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    selquery = "SELECT m.title, m.year, m.director, m.actor, m.rating FROM movies m WHERE rating = (SELECT MAX(rating) FROM movies m2);"
    message = ""
    try:
        cur.execute(selquery)
        cnx.commit()
    except (MySQLdb.Error, MySQLdb.Warning) as e:
        message = e
        messages.append(message)

    row = cur.fetchone()
    while row is not None:
        rowtext = ", ".join([str(x) for x in row])
        messages.append(rowtext)
        row = cur.fetchone()
    entries = query_data()
    return render_template('index.html', entries = entries, messages = messages)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()
    messages = []

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True)
    selquery = "SELECT m.title, m.year, m.director, m.actor, m.rating FROM movies m WHERE rating = (SELECT MIN(rating) FROM movies m2);"
    message = ""
    try:
        cur.execute(selquery)
        cnx.commit()
    except (MySQLdb.Error, MySQLdb.Warning) as e:
        message = e
        messages.append(message)
    row = cur.fetchone()
    while row is not None:
        rowtext = ", ".join([str(x) for x in row])
        messages.append(rowtext)
        row = cur.fetchone()
    entries = query_data()
    return render_template('index.html', entries = entries, messages = messages)


@app.route("/")
def hello():
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
